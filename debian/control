Source: mmg
Maintainer: Debian Science Maintainers <debian-science-maintainers@lists.alioth.debian.org>
Uploaders: Nico Schlömer <nico.schloemer@gmail.com>
Section: math
Priority: optional
Build-Depends: debhelper (>= 12),
  cmake,
  doxygen,
  graphviz,
  libscotch-dev
Standards-Version: 4.4.0
Vcs-Browser: https://salsa.debian.org/science-team/mmg
Vcs-Git: https://salsa.debian.org/science-team/mmg.git
Homepage: https://www.mmgtools.org

Package: libmmg-dev
Architecture: any
Section: libdevel
Depends: libmmg5 (= ${binary:Version}),
         libscotch-dev,
         ${misc:Depends},
         ${shlibs:Depends}
Description: Header files
 .
 This package provides the Mmg header files required to compile C programs that
 use Mmg.

Package: libmmg5
Architecture: any
Section: libs
Depends: ${misc:Depends}, ${shlibs:Depends}
Description: Shared libraries
 The libmmg libraries, i.e., libmmg2d, libmmgs, and libmmg3d.
 .
 This package provides the shared libraries needed to run C programs that use
 Mmg.

Package: mmg
Architecture: any
Depends: libmmg5 (= ${binary:Version}),
         ${misc:Depends},
         ${shlibs:Depends}
Description: Command-line Mmg applications
 This package provides 3 applications:
 * the mmg2d application and the libmmg2d library: adaptation and optimization
   of a two-dimensional triangulation and generation of a triangulation from a
   set of points or from given boundary edges
 * the mmgs application and the libmmgs library: adaptation and optimization of
   a surface triangulation and isovalue discretization
 * the mmg3d application and the libmmg3d library: adaptation and optimization
   of a tetrahedral mesh and implicit domain meshing
 .
 This package provides the Mmg applications.
